//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef PROJECT_DDPG_MODEL_H
#define PROJECT_DDPG_MODEL_H

#include <torch/torch.h>

/*
 *  Implementation of the actor and critic models. Both inherit from the Module class of libtorch.
 */

class ActorImpl : public torch::nn::Module
{
public:
    ActorImpl(int64_t stateSize, int64_t actionSize, int64_t fc1Units = 256, int64_t fc2Units = 256);

    //Initialise network parameters
    void resetParameters();

    //Returns output of the network given the input state
    torch::Tensor forward(torch::Tensor state);

    //Create options for the BN layer, where features is the number of features to be normalized.
    torch::nn::BatchNormOptions bnOptions(int64_t features);

    //Return the range [-lim, +lim] used to initialised weights of the NN
    std::pair<double, double> hiddenInit(torch::nn::Linear &layer);

    //Define linear layers
    torch::nn::Linear linAc1 { nullptr }, linAc2 { nullptr }, linAc3 { nullptr };
    //Define BatchNorm layers
    torch::nn::BatchNorm1d bnA1 { nullptr }, bnA2 { nullptr }, bnA3 { nullptr }, bnA4 { nullptr };
};

/******************* Critic *****************/

class CriticImpl : public torch::nn::Module
{
public:
    CriticImpl(int64_t stateSize, int64_t actionSize, int64_t fc1Units = 256, int64_t fc2Units = 256);

    //Initialise network parameters
    void resetParameters();

    //Returns output of the network given the input state and action
    torch::Tensor forward(torch::Tensor x, torch::Tensor action);

    //Create options for the BN layer, where features is the number of features to be normalized.
    torch::nn::BatchNormOptions bnOptions(int64_t features);

    //Return the range [-lim, +lim] used to initialised weights of the NN
    std::pair<double, double> hiddenInit(torch::nn::Linear &layer);

private:
    //Define linear layers
    torch::nn::Linear linCr1 { nullptr }, linCr2 { nullptr }, linCr3 { nullptr };
    //Define BatchNorm layers
    torch::nn::BatchNorm1d bnC1 { nullptr }, bnC2 { nullptr }, bnC3 { nullptr };
};

TORCH_MODULE (Actor);
TORCH_MODULE (Critic);

#endif //PROJECT_DDPG_MODEL_H
