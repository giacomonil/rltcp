//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "RLInterface.h"

namespace learning {

RLInterface::~RLInterface()
{
}

void RLInterface::setStringId(std::string _id)
{
    stringId = _id;
}

void RLInterface::setMaxObservationCount(int size)
{
    rlOldState.maxObservationsCount = size;
    rlState.maxObservationsCount = size;
}

void RLInterface::updateState(Observation obs)
{
    rlOldState = rlState;
    rlState.addObservation(obs);
}

void RLInterface::initialize(int stateSize, int maxObsSize)
{
    dataSignal = owner->registerSignal("stepData");
    querySignal = owner->registerSignal("actionQuery");

    delayWeightReward = owner->par("delayWeightReward");

    getSimulation()->getSystemModule()->subscribe("actionResponse", (cListener*) this);

    //Set the state size
    this->setStateSize(stateSize);

    //Set the max number of observations of a single state
    this->setMaxObservationCount(maxObsSize);

    for (int i = 0; i < maxObsSize; i++) {
        std::vector<float> vec;
        for (int j = 0; j < stateSize; j++)
            vec.push_back(0);
        Observation obs(vec);
        rlState.addObservation(obs);
        rlOldState.addObservation(obs);
    }
}

void RLInterface::setOwner(cComponent *_owner)
{
    owner = _owner;
}

// callback for signal response
void RLInterface::receiveSignal(cComponent *source, simsignal_t id, cObject *value, cObject *details)
{
    const char *signalName = owner->getSignalName(id);

    //Signal containing the response from the inference module.
    if (strcmp(signalName, "actionResponse") == 0) {
        Response *resp = dynamic_cast<Response*>(value);
        const string id_dest = resp->queryId;
        if (strcmp(id_dest.c_str(), stringId.c_str()) == 0) {
            float decision = resp->action;
            decisionMade(decision);
        }
    }
    else {
        EV_ERROR << "Unknown signal " << signalName << std::endl;
    }
}

int RLInterface::getStateSize() const
{
    return stateSize;
}

void RLInterface::setStateSize(int stateSize)
{
    this->stateSize = stateSize;
}

float RLInterface::computeReward(float delta, float delay, float throughput)
{
    if (isnan(delay) or isnan(throughput)) {
        throughput = 0;
        delay = 0;
        EV_WARN << "Delay or Throughput value is NaN during computation of the reward" << std::endl;
    }

    return throughput - delta * delay;
}

}
